require_relative 'contract'

class Amendment
  attr_reader :start_date

  def initialize(signature_date)
    @signature_date = signature_date
  end

  def amend_contract(contract, data = {})
    raise UnamendableContractException unless contract.confirmed?

    actual_contract = contract.actual
    signals = data[:signals] ? data[:signals] : actual_contract.signals
    contents = data[:contents] ? data[:contents] : actual_contract.contents
    repetitions = data[:repetitions] ? data[:repetitions] : actual_contract.repetitions
    frequency = data[:frequency] ? data[:frequency] : actual_contract.frequency
    amount = data[:amount] ? data[:amount] : actual_contract.amount
    client = contract.client

    new_contract = Contract.new(@signature_date, signals, contents, repetitions, frequency, amount, client)
    contract.add_amendment(new_contract)
  end
end
