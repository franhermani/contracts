class Contract
  attr_reader :signature_date, :signals, :contents, :repetitions,
              :frequency, :amount, :client, :amendments

  def initialize(signature_date, signals, contents, repetitions, frequency, amount, client)
    @signature_date = signature_date
    @signals = signals
    @contents = contents
    @repetitions = repetitions
    @frequency = frequency
    @amount = amount
    @client = client
    @confirmed = false
    @amendments = []
  end

  def add_amendment(amendment)
    @amendments << amendment
  end

  def quantity_amendments
    @amendments.length
  end

  def original
    self
  end

  def actual
    @amendments.empty? ? original : @amendments[@amendments.length - 1]
  end

  def confirm
    @confirmed = true
  end

  def confirmed?
    @confirmed
  end

  def modify(data = {})
    raise UnmodifiableContractException if self.confirmed?

    @signals = data[:signals] if data[:signals]
    @contents = data[:contents] if data[:contents]
    @repetitions = data[:repetitions] if data[:repetitions]
    @frequency = data[:frequency] if data[:frequency]
    @amount = data[:amount] if data[:amount]
  end
end
