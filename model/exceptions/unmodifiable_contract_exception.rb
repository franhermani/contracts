class UnmodifiableContractException < RuntimeError
  def initialize(msg = 'The contract cannot be modified because it has already been confirmed')
    super
  end
end