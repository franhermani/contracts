class UnamendableContractException < RuntimeError
  def initialize(msg = 'The contract cannot be amended because it has not been confirmed yet')
    super
  end
end