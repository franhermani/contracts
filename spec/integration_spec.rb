require 'rspec'
require_relative '../model/contract'
require_relative '../model/amendment'
require_relative '../model/exceptions/unmodifiable_contract_exception'
require_relative '../model/exceptions/unamendable_contract_exception'

describe Contract do
  let(:contract) { Contract.new('01-01-2019', ['volver', 'canal13'], ['Volver al futuro'],
                                1, 1, 10000, 'Artear') }
  let(:contract5) { Contract.new('01-01-2019', ['volver', 'canal13'], ['Volver al futuro'],
                                1, 1, 100, 'Artear') }
  let(:amendment) { Amendment.new('01-02-2019')}
  let(:amendment_2) { Amendment.new('01-03-2019')}

  it 'unconfirmed contract can be modified' do
    contract.modify({amount: 200000})
  end

  it 'confirmed contract cannot be modified' do
    contract.confirm
    expect { contract.modify({amount: 200000}) }
    .to raise_error(UnmodifiableContractException)
  end

  it 'unconfirmed contract can not be amended' do
    expect { amendment.amend_contract(contract, {amount: 2000}) }
    .to raise_error(UnamendableContractException)
  end

  it 'should have one amendment dated 01-02-2019 with new amount' do
    contract.confirm
    amendment.amend_contract(contract, {amount: 200000})
    expect(contract.quantity_amendments).to eq 1
    expect(contract.actual.signature_date).to eq '01-02-2019'
    expect(contract.actual.amount).to eq 200000
  end

  it 'should have two amendments dated 01-02-2019 and 01-03-2019 with new amount and repetitions, respectively' do
    contract5.confirm
    amendment.amend_contract(contract5, {amount: 50})
    amendment_2.amend_contract(contract5, {repetitions: 7})
    expect(contract5.quantity_amendments).to eq 2
    expect(contract5.actual.amount).to eq 50
    expect(contract5.original.amount).to eq 100
    expect(contract5.actual.repetitions).to eq 7
    expect(contract5.amendments[0].repetitions).to eq 1
    expect(contract5.amendments[1].repetitions).to eq 7
    expect(contract5.original.repetitions).to eq 1
    expect(contract5.actual.frequency).to eq 1
    expect(contract5.original.frequency).to eq 1
  end
end