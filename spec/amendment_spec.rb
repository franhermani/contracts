require 'rspec'
require_relative '../model/contract'
require_relative '../model/amendment'
require_relative '../model/exceptions/unamendable_contract_exception'

describe Amendment do
  let(:amendment) { Amendment.new('01-02-2019')}
  let(:contract) { Contract.new('01-01-2019', ['HBO'], ['Game of Thrones'],
                                10, 50, 10000, 'Artear') }

  it 'amendment cannot be applied to unconfirmed contract' do
    expect { amendment.amend_contract(contract, {signals: ['FOX', 'Warner Bros.']}) }
    .to raise_error(UnamendableContractException)
  end

  it 'amendment should modify only signal' do
    contract.confirm
    amendment.amend_contract(contract, {signals: ['FOX']})
    expect(contract.actual.signals).to eq ['FOX']
    expect(contract.actual.contents).to eq contract.contents
    expect(contract.actual.repetitions).to eq contract.repetitions
    expect(contract.actual.frequency).to eq contract.frequency
    expect(contract.actual.amount).to eq contract.amount
  end

  it 'amendment should modify only content' do
    contract.confirm
    amendment.amend_contract(contract, {contents: ['Breaking Bad']})
    expect(contract.actual.signals).to eq contract.signals
    expect(contract.actual.contents).to eq ['Breaking Bad']
    expect(contract.actual.repetitions).to eq contract.repetitions
    expect(contract.actual.frequency).to eq contract.frequency
    expect(contract.actual.amount).to eq contract.amount
  end

  it 'amendment should modify only repetitions' do
    contract.confirm
    amendment.amend_contract(contract, {repetitions: 5})
    expect(contract.actual.signals).to eq contract.signals
    expect(contract.actual.contents).to eq contract.contents
    expect(contract.actual.repetitions).to eq 5
    expect(contract.actual.frequency).to eq contract.frequency
    expect(contract.actual.amount).to eq contract.amount
  end

  it 'amendment should modify only frequency' do
    contract.confirm
    amendment.amend_contract(contract, {frequency: 25})
    expect(contract.actual.signals).to eq contract.signals
    expect(contract.actual.contents).to eq contract.contents
    expect(contract.actual.repetitions).to eq contract.repetitions
    expect(contract.actual.frequency).to eq 25
    expect(contract.actual.amount).to eq contract.amount
  end

  it 'amendment should modify only amount' do
    contract.confirm
    amendment.amend_contract(contract, {amount: 25000})
    expect(contract.actual.signals).to eq contract.signals
    expect(contract.actual.contents).to eq contract.contents
    expect(contract.actual.repetitions).to eq contract.repetitions
    expect(contract.actual.frequency).to eq contract.frequency
    expect(contract.actual.amount).to eq 25000
  end
end
