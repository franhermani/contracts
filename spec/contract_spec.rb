require 'rspec'
require_relative '../model/contract'
require_relative '../model/amendment'
require_relative '../model/exceptions/unmodifiable_contract_exception'

describe Contract do
  let(:contract) { Contract.new('01-01-2019', ['HBO'], ['Game of Thrones'],
                                10, 50, 10000, 'Artear') }
  let(:amendment) { Amendment.new('01-02-2019')}
  let(:amendment_2) { Amendment.new('01-03-2019')}
  let(:amendment_3) { Amendment.new('01-04-2019')}

  it 'unconfirmed contract can be modified' do
    expect(contract.actual.signals).to eq ['HBO']
    expect(contract.actual.contents).to eq ['Game of Thrones']
    contract.modify({signals: ['FOX', 'TV Publica'], contents: ['How I Met Your Mother']})
    expect(contract.actual.signals).to eq ['FOX', 'TV Publica']
    expect(contract.actual.contents).to eq ['How I Met Your Mother']
  end

  it 'confirmed contract cannot be modified' do
    contract.confirm
    expect { contract.modify({signals: ['FOX', 'TV Publica'], contents: ['How I Met Your Mother']}) }
    .to raise_error(UnmodifiableContractException)
  end

  it 'actual contract should be the same as original contract' do
    expect(contract.quantity_amendments).to eq 0
    expect(contract.actual.signature_date).to eq contract.original.signature_date
    expect(contract.actual.contents).to eq contract.original.contents
    expect(contract.actual.repetitions).to eq contract.original.repetitions
    expect(contract.actual.frequency).to eq contract.original.frequency
    expect(contract.actual.amount).to eq contract.original.amount
  end

  it 'should have one amendment dated 01-02-2019 with new content' do
    contract.confirm
    amendment.amend_contract(contract, {contents: ['Stranger Things']})
    expect(contract.quantity_amendments).to eq 1
    expect(contract.actual.signature_date).to eq '01-02-2019'
    expect(contract.actual.contents).to eq ['Stranger Things']
  end

  it 'should have two amendments dated 01-02-2019 and 01-03-2019 with new content and repetitions, respectively' do
    contract.confirm
    amendment.amend_contract(contract, {contents: ['Stranger Things']})
    amendment_2.amend_contract(contract, {repetitions: 35})
    expect(contract.quantity_amendments).to eq 2
    expect(contract.amendments[0].signature_date).to eq '01-02-2019'
    expect(contract.amendments[0].contents).to eq ['Stranger Things']
    expect(contract.amendments[1].signature_date).to eq '01-03-2019'
    expect(contract.amendments[1].repetitions).to eq 35
  end

  it 'actual contract should have new content, repetitions and cost' do
    contract.confirm
    amendment.amend_contract(contract, {contents: ['Friends'], repetitions: 15, amount: 200000})
    expect(contract.actual.contents).to eq ['Friends']
    expect(contract.actual.repetitions).to eq 15
    expect(contract.actual.amount).to eq 200000
  end

  it 'actual contract should carry over every amendment' do
    contract.confirm
    amendment.amend_contract(contract, {contents: ['Friends']})
    amendment_2.amend_contract(contract, {frequency: 100})
    amendment_3.amend_contract(contract, {amount: 200000})
    expect(contract.quantity_amendments).to eq 3
    expect(contract.actual.contents).to eq ['Friends']
    expect(contract.actual.frequency).to eq 100
    expect(contract.actual.amount).to eq 200000
  end
end
